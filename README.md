# Apple Mac DevOps Build

This document will explain how to set up an out of the box Mac device so that it can function within the DevOps team.

---

## Initial Startup

Follow the on screen instruction when you first power up the device.

---

## Connecting to Git through command line

It may be easier to create new keys instead of copying the ones over from windows. To do so do the following.

```bash
ssh-keygen -t rsa -b 2048 -C "<Your email address>"
cat ~/.ssh/id_rsa.pub | pbcopy
```

Open up [GitHub](https://github.com/) / [GitLab](https://gitlab.com/) and navigate to the keys in your profile. Click on Add SSH Key, paste in the contents and save.

You should now be able to checkout/commit into Git using the SSH stanza.

```bash
mkdir -p ~/git/gibbsoft
cd ~/git/gibbsoft
git clone git@gitlab.com:gibbsoft/osx.git
```

> 1) If you are prompted to install the OSX Command-line tools from Apple then please do so, and then try the git command again.
> 2) If you would rather use the HTTPS stanza, the repo URL is `https://gitlab.com/gibbsoft/osx.git`
---

## Install required DevOps software

First install needs to be `virtualenv`. To install run the following command...

```bash
cd osx
easy_install virtualenv
sudo virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
ansible-playbook build.yaml
```

Xcode command-line tools are installed as part of the process and the first run will fail.  Simply run the last two steps again.
